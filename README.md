# 前言
本文主要介绍SpringCloud使用Nacos当配置中心和注册中心，通过过一个简单的Demo学习Naocs的基本配置以及不同微服务通过注册中心之前的相互调用。

# Nacos的安装配置
Nacos的学习资源主要有以下两个

1. Nacos管网 [Nacos管网](https://nacos.io/zh-cn/docs/v2/quickstart/quick-start.html)
2. Nacos GitHub [NacosGitHub](https://github.com/alibaba/nacos/)

## 下载Nacos
直接从GitHub [https://github.com/alibaba/nacos/releases](https://github.com/alibaba/nacos/releases)下载对应版本的Nacos，本使用2.1.0，没有GitHub加速的可以直接从CSDN下载，我上传的，完全免费，不需要积分[https://download.csdn.net/download/whzhaochao/88074308](https://download.csdn.net/download/whzhaochao/88074308)

## 启动Naocs
如果是Windows 修改`startup.cmd`中的配置`set MODE="standalone"`配置成单机模式，然后点击`startup.cmd`运行就可以启用Nacos了，也可以使用`startup.sh -m standalone` 指定启动模式。

![在这里插入图片描述](https://img-blog.csdnimg.cn/828e2b8396c34af3bd968d79f85e56c9.png)
当我们看到 8848后，说明启动成功，浏览器输入[http://127.0.0.1:8848/nacos/](http://127.0.0.1:8848/nacos/)，用户名nacos，密码nacos就可以登录进入Nacos了。

## 配置nacos
![在这里插入图片描述](https://img-blog.csdnimg.cn/2be683d2def84c7183e3ff96f15bfb3f.png)
进入Nacos后我可有几个概率需要明确一下

###  命名空间
可以理解成不同的环境，比如dev/test/prod对应开发/测试/正式，我们可以分别创建这三个环境，注意命名空间ID(不填则自动生成)我们就不填，让他自动生成好了。
![在这里插入图片描述](https://img-blog.csdnimg.cn/626afb542eb449be9946401cfa8d7cca.png)
## 配置列表
配置列表就是替代我们以前项目的中yml或者properties，比如我们用个用户中心，我们选择dev环境创建一个DataID为user-service Group为Shop，这里DataID可以对应我们的一个微服务，Group可以理解成我们的一个项目，我们做一个商城，会有用户中心，订单中心。这里商城这个项目可以理解成Group，用户中心可以理解成Data ID。

![在这里插入图片描述](https://img-blog.csdnimg.cn/8776d25249864fbbaeaefa139ebf64b6.png)

## 服务管理
这里服务管理就对应我们的一个微服务，当我们的微服务启动后会自动注册到这里来，目前还没有数据

# SpringCloud环境搭建
我们做一个非常简单的Demo，基本逻辑就是启用一个用户中心，里面从nacos配置中心拉取用户配置，然后对应提供一个接口通过ID获取用户信息，在订单中心中创建一个接口，调用用户中心的接口获取用户信息，基本项目架构如下图：
![在这里插入图片描述](https://img-blog.csdnimg.cn/08e251686b514a61ad23f9eb4704b39a.png)
## 用户中心的 bootstrap.yml配置
这里我们主要看两个关键配置nacos.config和nacos.discovery， config主要是配置从nacos哪里拉取配置信息，这里我们prefix就对应用我们nacos后台的user-service,namespace对应我们的开发环境，group就对应我们的项目shop。
```bash
server:
  port: 82
spring:
  profiles:
    active: dev
  application:
    name: user-service
  cloud:
    nacos:
      config:
        server-addr: 127.0.0.1:8848
        group: shop
        enabled: true
        prefix: user-service
        namespace: 6835622b-e819-4a78-a4d0-3fdc12b29ae9
        file-extension: yaml
      discovery:
        server-addr: 127.0.0.1:8848
        username: nacos
        password: nacos
        namespace: 6835622b-e819-4a78-a4d0-3fdc12b29ae9
        group: shop
```

我们看一下`UserConfig`这个类，通过这个类我们可以拉取nacos中配置的users.list数据。

```bash
@Configuration
@ConfigurationProperties("users")
@Data
public class UserConfig {
	private List<User> list;
}
```
我们看一下`UserController` 这个类提供了一个`/user/{id}`通过id获取nacos配置中心的用户信息。
```bash
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Value("${loginUser:admin}")
    private String loginUser;
    @Resource
    private UserConfig userConfig;
    @GetMapping("/{id}")
    public User info(@PathVariable Long id){
        log.info("loginUser:{}",loginUser);
        return  userConfig.getList().stream().filter(x->x.getId().equals(id)).findFirst().get();
    }
}
```

启用nacos-user项目，当我们启动nacos-user项目后，在nacos后台可以看到user-service,端口这82![在这里插入图片描述](https://img-blog.csdnimg.cn/b9a733b09f5241d0a29ab9d48e017530.png)

我们通过nacos上显示的IP及端口 [http://192.168.1.103:82/user/1](http://192.168.1.103:82/user/1)，可以成功的获取nacos的配置信息
![在这里插入图片描述](https://img-blog.csdnimg.cn/c3945b29c9344cbfb87c55521dc64817.png)


## 订单中心
订单中心配置
```bash
server:
  port: 81

spring:
  profiles:
    active: dev
  application:
    name: order-service
  cloud:
    nacos:
      config:
        server-addr: 127.0.0.1:8848
        group: shop
        enabled: true
        prefix: order-service
        namespace: 6835622b-e819-4a78-a4d0-3fdc12b29ae9
        file-extension: yaml
      discovery:
        server-addr: 127.0.0.1:8848
        username: nacos
        password: nacos
        namespace: 6835622b-e819-4a78-a4d0-3fdc12b29ae9
        group: shop
```
订单中心，在订单中心我们写了两个接口`/order/{id}`通过`restTempate`调用用户中心的接口和`/order/feign/{id}`通过openFeign调用用户中心的接口
```bash
@RestController
@RequestMapping("/order")
public class OrderController {
    @Resource
    private RestTemplate restTemplate;
    @Resource
    private UserFeign userFeign;
    @GetMapping("/{id}")
    public User info(@PathVariable Long id){
       return restTemplate.getForObject("http://user-service/user/".concat(id.toString()), User.class);
    }
    @GetMapping("/feign/{id}")
    public User feign(@PathVariable Long id){
        return userFeign.getUser(id);
    }
}
@Configuration
public class RestTempleteConfig {
    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}

@FeignClient(value = "user-service")
@Component
public interface UserFeign {
    @GetMapping("user/{id}")
    User getUser(@PathVariable Long id);
}
```

启动nacos-order ，在nacos的服务中会有一个order-service，我们通过IP及端口访问对应接口，完成对用户中心服务的调用
![在这里插入图片描述](https://img-blog.csdnimg.cn/59a78e21506c473ea288827579e90e58.png)
输入[http://192.168.1.103:81/order/1](http://192.168.1.103:81/order/1)通过restTempate调用用户中心接口
输入[http://192.168.1.103:81/order/feign/2](http://192.168.1.103:81/order/feign/2)通过openFeign调用用户中心接口

![在这里插入图片描述](https://img-blog.csdnimg.cn/dfc8af98069a4e08ac5161f47ec95714.png)

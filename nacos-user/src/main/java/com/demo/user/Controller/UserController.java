package com.demo.user.Controller;

import com.demo.user.config.UserConfig;
import com.nacos.base.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @Description: TODO
 * @Author 赵侠客
 * @Date 2023/7/16
 **/
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Value("${server.port}")
    private Integer port;
    @Resource
    private UserConfig userConfig;
    @GetMapping("/{id}")
    public User info(@PathVariable Long id){
        return  userConfig.getList().stream().filter(x->x.getId().equals(id)).peek(x-> x.setPort(port)).findFirst().get();
    }
}

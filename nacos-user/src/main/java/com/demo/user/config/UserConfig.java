package com.demo.user.config;

import com.nacos.base.model.User;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties("users")
@Data
public class UserConfig {
	private List<User> list;
}
package com.demo.order.controller;

import com.demo.order.feign.UserFeign;
import com.nacos.base.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @Description: TODO
 * @Author 赵侠客
 * @Date 2023/7/16
 **/
@RestController
@RequestMapping("/order")
public class OrderController {
    @Resource
    private RestTemplate restTemplate;
    @Resource
    private UserFeign userFeign;
    @GetMapping("/{id}")
    public User info(@PathVariable Long id){
       return restTemplate.getForObject("http://user-service/user/".concat(id.toString()), User.class);
    }
    @GetMapping("/feign/{id}")
    public User feign(@PathVariable Long id){
        return userFeign.getUser(id);
    }
}

package com.demo.order.feign;
 
 
import com.nacos.base.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "user-service")
@Component
public interface UserFeign {
    @GetMapping("user/{id}")
    User getUser(@PathVariable Long id);
}
package com.nacos.base.model;

import lombok.Data;

/**
 * @Description: TODO
 * @Author 赵侠客
 * @Date 2023/7/16
 **/
@Data
public class User {
    private Long id;
    private Integer age;
    private String name;
    private Integer port;
}
